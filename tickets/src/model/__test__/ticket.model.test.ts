import { Ticket } from "../index";
it("implements optimistic concurrency control", async () => {
  const test = Ticket.build({
    price: 12,
    title: "dede",
    userId: "dede",
  });

  await test.save();

  const firstInstance = await Ticket.findById(test.id);
  const scdInstance = await Ticket.findById(test.id);

  firstInstance?.set({ price: 19 });
  scdInstance?.set({ price: 9 });

  await firstInstance?.save();

  try {
    await scdInstance?.save();
  } catch (e: any) {
    return;
  }

  throw new Error("should not reach this point");
});
it("increments version number on save", async () => {
  const test = Ticket.build({
    price: 12,
    title: "dede",
    userId: "dede",
  });

  await test.save();

  expect(test.version).toEqual(0);
  await test.save();
  expect(test.version).toEqual(1);

  await test.save();
  expect(test.version).toEqual(2);
  //TODO fix test
});
