import mongoose from "mongoose";
import { server } from "./app";
import { OrderCancelledEventListener } from "./events/listener/order-cancelled-listener";
import { OrderCreatedListener } from "./events/listener/order-created-listener";
import { natsWrapper } from "./nats-wrapper";
async function start() {
  try {
    if (!process?.env?.jwt) {
      return new Error("env not set");
    }
    if (!process?.env?.MONGO_URI) {
      return new Error("mongo uri not set");
    }
    if (!process?.env?.NATS_URL) {
      return new Error("NATS_URL not set");
    }
    if (!process?.env?.NATS_CLUSTER_ID) {
      return new Error("NATS_CLUSTER_IDnot set");
    }
    if (!process?.env?.NATS_CLIENT_ID) {
      return new Error("NATS_CLIENT_ID not set");
    }
    try {
      await natsWrapper.connect(
        process.env.NATS_CLUSTER_ID,
        process.env.NATS_CLIENT_ID,
        process.env.NATS_URL
      );

      natsWrapper.client.on("close", () => {
        console.log("closing");
        process.exit();
      });
      process.on("SIGINT", () => natsWrapper.client.close());
      process.on("SIGTERM", () => natsWrapper.client.close());

      new OrderCancelledEventListener(natsWrapper.client).listen();
      new OrderCreatedListener(natsWrapper.client).listen();
      await mongoose.connect(process.env.MONGO_URI);
    } catch (e) {
      console.error(e);
    }

    server.listen(3000, "0.0.0.0", (err, address) => {
      if (err) {
        console.error(err);
        process.exit(1);
      }
      console.log(`tickets server listening at ${address} port 3000.`);
    });
  } catch (e) {
    throw e;
  }
}
start();
