import { protectedRoute, throwCustomError } from "@kylab/msvc_shared";
import {
  FastifyInstance,
  FastifyRegisterOptions,
  FastifyReply,
  FastifyRequest,
} from "fastify";
import { TicketUpdatedPublisher } from "../events/publisher/ticket-updated-publisher";
import { Ticket } from "../model";
import { natsWrapper } from "./../nats-wrapper";
interface TicketPayload {
  price: number;
  title: string;
}
type UpdateTicketsRequest = FastifyRequest<{ Body: TicketPayload }>;
export function updateTicketsRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  const schema = {
    body: {
      type: "object",
      required: ["title", "price"],
      properties: {
        title: { type: "string", minLength: 1 },
        price: { type: "number", minimum: 1 },
      },
    },
  };

  fastify.addHook("preHandler", protectedRoute);

  fastify.put(
    "/api/tickets/:id",
    { schema, attachValidation: false },
    async (request: UpdateTicketsRequest, reply: FastifyReply) => {
      const { title, price } = request.body;
      try {
        let ticket = await Ticket.findById((request.params as any).id);
        if (!ticket) {
          throwCustomError("not Found", "notFound", 404);
          return;
        }

        if (ticket.orderId) {
          throwCustomError("Ticket is reserved", "BadRequest", 400);
          return;
        }

        if ((request as any).activeUser.id !== ticket?.userId) {
          throwCustomError("User not found", "Auth", 401);
          return;
        }

        ticket.set({
          title,
          price,
        });
        await ticket.save();

        await new TicketUpdatedPublisher(natsWrapper.client).publish({
          id: ticket.id,
          title: ticket.title,
          price: ticket.price,
          userId: ticket.userId,
          version: ticket.version + 1,
        });

        reply.status(200).send(ticket);
      } catch (e) {
        return e;
      }
    }
  );

  done();
}
