import mongoose from "mongoose";
import { server } from "../../app";
import { Ticket } from "./../../model/index";
import { natsWrapper } from "./../../nats-wrapper";
//TODO DEMOCK
const MOCK =
  "my-session-cookie=Pvh2aybg45ZgV6MKruuNW68JVLXi31Jjz5t299m9sCs1R0fxROWSCYaWlZ7aeisBknfqlnPylxbeFgqMJ8V3k2Qlgjrz7enx9psjcP1cyNMXzuF%2BLCDQ%2FgRZ3tptwinGXEaS4xZziNPX%2Ffd3kAczcWjSn7aZWWANqAPc8pI6YEbkXBDfu1it%2Bxyj895ZOgr3P3QRy02v4AYK%2FMady1whaMsIEwDvHd%2FqoQEOVJnCPBl7ERldXPK2BaznNLM4jGVn3QPvCVysySO%2BOmeY9Qvx28ew8A%3D%3D%3Bp36myo%2BoJzgpPp2e%2BOYI2EDniEpRWHeK; Path=/; SameSite=Lax";
const id = new mongoose.Types.ObjectId().toHexString();
describe("Update tickets", () => {
  it("return 404 if id does not exist", async () => {
    const resp = await server.inject({
      method: "PUT",
      url: "/api/tickets/".concat(id),
      headers: {
        cookie: MOCK,
      },
      payload: {
        title: "dfrfr",
        price: 23,
      },
    });

    expect(resp.statusCode).toBe(404);
  });
  it("return 401 if user does not exist", async () => {
    const resp = await server.inject({
      method: "PUT",
      url: "/api/tickets/".concat(id),
      payload: {
        title: "dfrfr",
        price: 23,
      },
    });

    expect(resp.statusCode).toBe(401);
  });
  //TODO
  /*   it("return 401 if user does own the tickets", async () => {
    await (global as any).signin("629dc0743e66d46e6607de30");
    const MOCK1 =
      "my-session-cookie=aprV6ys%2BmXmBlArKN6sslawjDUujtl2L84nSE6MzrSrC74%2Bl9DuFKKfHozcGS%2Fud0DZEIyEQFKg%2F1LEgWwRQSZyKMsvRr%2FYI8I%2BvFvIAlpn6VnA0LRTKCzDfHBEIhm8dXBFNWtQYhGFCLqvrrRO%2FAy8DvCcNoMBhK4IhXgffd0slChV0wG3Ien581k18vYzUCNifa81f7qtDPY%2BVG4Vk9BJeLCf6%2BDniClX49gdHcCWuxesj9x1BWZ3%2F2Wle9BfYMZt07AhlIlYLP%2BVIehnKBkFDwjkhcryopTNMWwDndg%3D%3D%3B2iVkBGrmqh3avhYj%2BOUxnJTyRaFUBcJm; Path=/; SameSite=Lax";
    const t = await server.inject({
      method: "POST",
      url: "/api/tickets",
      payload: {
        title: "dfrfr",
        price: 23,
      },
      headers: {
        cookie: MOCK1,
      },
    });
	const respCreate = JSON.parse(create.body);

    console.log(t.body, "KKKkKKKK");
    await global.signin();
    const resp = await server.inject({
      method: "PUT",
      url: "/api/tickets/".concat(respCreate.id),
      payload: {
        title: "dfrfr",
        price: 23,
      },
      headers: {
        cookie: MOCK,
      },
    });

    expect(resp.statusCode).toBe(401);
  }); */
  it("return 400 if user provide invalid title or price", async () => {
    const resp = await server.inject({
      method: "PUT",
      url: "/api/tickets/".concat(id),
      payload: {},
      headers: {
        cookie: MOCK,
      },
    });

    expect(resp.statusCode).toBe(400);
  });

  it("updates ticket provide valid title or price", async () => {
    const create = await server.inject({
      method: "POST",
      url: "/api/tickets/",
      payload: {
        title: "dfrfr",
        price: 23,
      },
      headers: {
        cookie: MOCK,
      },
    });

    const respCreate = JSON.parse(create.body);

    const resp = await server.inject({
      method: "PUT",
      url: "/api/tickets/".concat(respCreate.id),
      headers: {
        cookie: MOCK,
      },
      payload: {
        title: "changed",
        price: 45,
      },
    });

    expect(resp.statusCode).toBe(200);

    const ticket = await server.inject({
      method: "GET",
      url: "/api/tickets/".concat(respCreate.id),
      headers: {
        cookie: MOCK,
      },
    });
    const ticketBody = JSON.parse(ticket.body);
    expect(ticketBody.title).toBe("changed");
    expect(ticketBody.price).toBe(45);
  });

  it("publish events", async () => {
    const create = await server.inject({
      method: "POST",
      url: "/api/tickets/",
      payload: {
        title: "dfrfr",
        price: 23,
      },
      headers: {
        cookie: MOCK,
      },
    });

    const respCreate = JSON.parse(create.body);
    const resp = await server.inject({
      method: "PUT",
      url: "/api/tickets/".concat(respCreate.id),
      headers: {
        cookie: MOCK,
      },
      payload: {
        title: "changed",
        price: 45,
      },
    });
    expect(natsWrapper.client.publish).toHaveBeenCalled();
  });

  it("rejects ticket if orderid is set", async () => {
    const create = await server.inject({
      method: "POST",
      url: "/api/tickets/",
      payload: {
        title: "dfrfr",
        price: 23,
      },
      headers: {
        cookie: MOCK,
      },
    });

    const respCreate = JSON.parse(create.body);

    const ticket = await Ticket.findById(respCreate.id);

    ticket!.set({ orderId: new mongoose.Types.ObjectId().toHexString() });

    await ticket!.save();
    const resp = await server.inject({
      method: "PUT",
      url: "/api/tickets/".concat(respCreate.id),
      headers: {
        cookie: MOCK,
      },
      payload: {
        title: "changed",
        price: 45,
      },
    });

    expect(resp.statusCode).toBe(400);
  });
});
