import { server } from "../../app";
import { Ticket } from "../../model";
import { natsWrapper } from "./../../nats-wrapper";
//TODO DEMOCK
const MOCK =
  "my-session-cookie=Pvh2aybg45ZgV6MKruuNW68JVLXi31Jjz5t299m9sCs1R0fxROWSCYaWlZ7aeisBknfqlnPylxbeFgqMJ8V3k2Qlgjrz7enx9psjcP1cyNMXzuF%2BLCDQ%2FgRZ3tptwinGXEaS4xZziNPX%2Ffd3kAczcWjSn7aZWWANqAPc8pI6YEbkXBDfu1it%2Bxyj895ZOgr3P3QRy02v4AYK%2FMady1whaMsIEwDvHd%2FqoQEOVJnCPBl7ERldXPK2BaznNLM4jGVn3QPvCVysySO%2BOmeY9Qvx28ew8A%3D%3D%3Bp36myo%2BoJzgpPp2e%2BOYI2EDniEpRWHeK; Path=/; SameSite=Lax";
describe("Create tickets", () => {
  it("has a route tickets api/tickets post", async () => {
    const resp = await server.inject({
      method: "POST",
      url: "/api/tickets/",
      payload: {},
    });

    expect(resp.statusCode).not.toBe(404);
  });
  it("can not be accessed if the user is not signedin", async () => {
    const resp = await server.inject({
      method: "POST",
      url: "/api/tickets/",
      payload: { title: "A", price: 1 },
    });

    expect(resp.statusCode).toBe(401);
  });
  it("can be accessed if the user is signedin", async () => {
    const cookie = await global.signin();
    //TODO demock

    const resp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/tickets",
      payload: { title: "", price: 0 },
    });

    expect(resp.statusCode).not.toBe(401);
  });

  it("retuns an error if title provided is invalid", async () => {
    const cookie = await global.signin();
    //TODO demock

    const resp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/tickets/",
      payload: { title: "", price: 12 },
    });

    expect(resp.statusCode).toBe(400);
  });
  it("retuns an error if title is not provided ", async () => {
    const cookie = await global.signin();

    const resp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/tickets/",
      payload: { price: 0 },
    });

    expect(resp.statusCode).toBe(400);
  });

  it("retuns an error if price provided is invalid", async () => {
    const cookie = await global.signin();
    //TODO demock

    const resp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/tickets/",
      payload: { title: "test", price: -10 },
    });

    expect(resp.statusCode).toBe(400);
  });
  it("retuns an error if price is not provided", async () => {
    const cookie = await global.signin();
    //TODO demock

    const resp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/tickets/",
      payload: { title: "test" },
    });

    expect(resp.statusCode).toBe(400);
  });
  it("creates tickets on valid tickets", async () => {
    let tickets = await Ticket.find({});

    expect(tickets.length).toEqual(0);

    const cookie = await global.signin();
    //TODO demock

    const resp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/tickets/",
      payload: { title: "test", price: 12.5 },
    });

    tickets = await Ticket.find({});
    expect(tickets.length).toEqual(1);
  });

  it("publishes event", async () => {
    let tickets = await Ticket.find({});

    expect(tickets.length).toEqual(0);

    const cookie = await global.signin();
    //TODO demock

    const resp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/tickets/",
      payload: { title: "test", price: 12.5 },
    });

    expect(natsWrapper.client.publish).toHaveBeenCalled();
  });
});
