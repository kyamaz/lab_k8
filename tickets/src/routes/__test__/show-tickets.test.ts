import mongoose from "mongoose";
import { server } from "../../app";

//TODO DEMOCK
const MOCK =
  "my-session-cookie=Pvh2aybg45ZgV6MKruuNW68JVLXi31Jjz5t299m9sCs1R0fxROWSCYaWlZ7aeisBknfqlnPylxbeFgqMJ8V3k2Qlgjrz7enx9psjcP1cyNMXzuF%2BLCDQ%2FgRZ3tptwinGXEaS4xZziNPX%2Ffd3kAczcWjSn7aZWWANqAPc8pI6YEbkXBDfu1it%2Bxyj895ZOgr3P3QRy02v4AYK%2FMady1whaMsIEwDvHd%2FqoQEOVJnCPBl7ERldXPK2BaznNLM4jGVn3QPvCVysySO%2BOmeY9Qvx28ew8A%3D%3D%3Bp36myo%2BoJzgpPp2e%2BOYI2EDniEpRWHeK; Path=/; SameSite=Lax";
describe("Show ticket", () => {
  it("return 404 if ticket is not found", async () => {
    try {
      const resp = await server.inject({
        method: "GET",
        url: "/api/tickets/rerer",
        headers: {
          cookie: MOCK,
        },
      });
      expect(resp.statusCode).toBe(404);
    } catch (e: any) {
      expect(e.statusCode).toBe(404);
    }
  });

  it("return 404 if ticket id  is not found", async () => {
    try {
      const id = new mongoose.Types.ObjectId().toHexString();
      const resp = await server.inject({
        method: "GET",
        url: "/api/tickets/".concat(id),
        headers: {
          cookie: MOCK,
        },
      });
      expect(resp.statusCode).toBe(404);
    } catch (e: any) {
      expect(e.statusCode).toBe(404);
    }
  });
  it("return ticket if ticket is found", async () => {
    const response = await server.inject({
      method: "POST",
      url: "/api/tickets/",
      headers: {
        cookie: MOCK,
      },

      payload: {
        title: "test",
        price: 666,
      },
    });

    const respJ = JSON.parse(response.body);

    const ticketResp = await server.inject({
      method: "GET",
      url: "/api/tickets/".concat(respJ.id),
      headers: {
        cookie: MOCK,
      },
    });

    expect(ticketResp.statusCode).toBe(200);
    expect(respJ.title).toBe("test");
    expect(respJ.price).toBe(666);
  });
});
