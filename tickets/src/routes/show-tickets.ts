import {
  FastifyInstance,
  FastifyRegisterOptions,
  FastifyReply,
  FastifyRequest,
} from "fastify";
import { Ticket } from "../model";
import { throwCustomError } from "@kylab/msvc_shared";
import { protectedRoute } from "@kylab/msvc_shared";

type ShowTicketsRequest = FastifyRequest;
export function showTicketsRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  fastify.addHook("preHandler", protectedRoute);
  fastify.get(
    "/api/tickets/",
    async (request: ShowTicketsRequest, reply: FastifyReply) => {
      try {
        let tickets = await Ticket.find({});
        if (!tickets) {
          throwCustomError("not found", "NotFound", 404);
        }

        reply.status(200).send(tickets);
      } catch (e) {
        throwCustomError("not found", "NotFound", 404);
      }
    }
  );

  fastify.get(
    "/api/tickets/:id",
    async (request: ShowTicketsRequest, reply: FastifyReply) => {
      const { id } = request.params as any;

      try {
        let ticket = await Ticket.findById(id);
        if (!ticket) {
          throwCustomError("not found", "NotFound", 404);
        }

        reply.status(200).send(ticket);
      } catch (e) {
        throwCustomError("not found", "NotFound", 404);
      }
    }
  );

  done();
}
