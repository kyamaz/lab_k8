import { protectedRoute } from "@kylab/msvc_shared";
import {
  FastifyInstance,
  FastifyRegisterOptions,
  FastifyReply,
  FastifyRequest,
} from "fastify";
import { TicketCreatedPublisher } from "../events/publisher/ticket-created-publisher";
import { Ticket } from "../model";
import { natsWrapper } from "./../nats-wrapper";
interface TicketPayload {
  price: number;
  title: string;
}
type CreateTicketsRequest = FastifyRequest<{ Body: TicketPayload }>;
export function createTicketsRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  const schema = {
    body: {
      type: "object",
      required: ["title", "price"],
      properties: {
        title: { type: "string", minLength: 1 },
        price: { type: "number", minimum: 1 },
      },
    },
  };

  fastify.addHook("preHandler", protectedRoute);

  fastify.post(
    "/api/tickets/",
    { schema, attachValidation: false },
    async (request: CreateTicketsRequest, reply: FastifyReply) => {
      const { title, price } = request.body;

      let ticket = Ticket.build({
        title,
        price,
        userId: (request as any)?.activeUser?.id,
      });
      await ticket.save();

      await new TicketCreatedPublisher(natsWrapper.client).publish({
        id: ticket.id,
        title: ticket.title,
        price: ticket.price,
        userId: ticket.userId,
        version: 0,
      });
      reply.status(201).send(ticket);
    }
  );

  done();
}
