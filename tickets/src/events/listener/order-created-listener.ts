import { Listener, OrderCreatedEvent, Subjects } from "@kylab/msvc_shared";
import { Message } from "node-nats-streaming";
import { Ticket } from "../../model/index";
import { TicketUpdatedPublisher } from "./../publisher/ticket-updated-publisher";
export const QUEUE_GRP_NAME = "tickets-service";
export class OrderCreatedListener extends Listener<OrderCreatedEvent> {
  readonly subject: Subjects.OrderCreated = Subjects.OrderCreated;
  queueGroupName = QUEUE_GRP_NAME;
  async onMsg({ id, ticket }: OrderCreatedEvent["data"], msg: Message) {
    const _ticket = await Ticket.findById(ticket.id);

    if (!_ticket) {
      throw new Error("ticket not found");
    }

    _ticket.set({ orderId: id });

    await _ticket.save();

    await new TicketUpdatedPublisher(this.client).publish({
      id: _ticket.id,
      title: _ticket.title,
      price: _ticket.price,
      userId: _ticket.userId,
      version: _ticket.version,
      orderId: id,
    });

    msg.ack();
  }
}
