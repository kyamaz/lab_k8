import { OrderCreatedEvent, OrderStatus } from "@kylab/msvc_shared";
import mongoose from "mongoose";
import { Message } from "node-nats-streaming";
import { natsWrapper } from "../../../nats-wrapper";
import { OrderCreatedListener } from "../order-created-listener";
import { Ticket } from "./../../../model/index";
async function setup() {
  const listener = new OrderCreatedListener(natsWrapper.client);
  const userId = new mongoose.Types.ObjectId().toHexString();

  const ticket = Ticket.build({
    title: "dede",
    price: 77,
    userId,
  });
  await ticket.save();

  const data: OrderCreatedEvent["data"] = {
    version: 0,
    id: new mongoose.Types.ObjectId().toHexString(),
    userId,
    status: OrderStatus.Created,
    expiresAt: new Date().toUTCString(),
    ticket: {
      id: ticket.id,
      price: ticket.price,
    },
  };

  const msg: Partial<Message> = {
    ack: jest.fn(),
  };

  return {
    listener,
    data,
    msg,
  };
}
describe("Order created event listener", () => {
  it("sets orderId of ticketid", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);
    const updatedTicket = await Ticket.findById(data.ticket.id);

    expect(updatedTicket).toBeDefined();

    expect(updatedTicket?.orderId).toBe(data.id);
  });
  it("acks the msg", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);

    expect(msg.ack).toHaveBeenCalled();
  });

  it("publishes ticket event", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);

    expect(natsWrapper.client.publish).toHaveBeenCalled();
    const mock = (natsWrapper.client.publish as jest.Mock).mock.calls;
    const evPayload = JSON.parse(mock[0][1]);

    expect(data.id).toBe(evPayload.orderId);
  });
});
