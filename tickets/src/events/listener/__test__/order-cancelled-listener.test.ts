import { OrderCancelledEvent } from "@kylab/msvc_shared";
import mongoose from "mongoose";
import { Message } from "node-nats-streaming";
import { Ticket } from "../../../model/index";
import { natsWrapper } from "../../../nats-wrapper";
import { OrderCancelledEventListener } from "./../order-cancelled-listener";
async function setup() {
  const listener = new OrderCancelledEventListener(natsWrapper.client);
  const userId = new mongoose.Types.ObjectId().toHexString();
  const orderId = new mongoose.Types.ObjectId().toHexString();
  const ticket = Ticket.build({
    title: "dede",
    price: 77,
    userId,
  });

  ticket.set({ orderId });
  await ticket.save();

  const data: OrderCancelledEvent["data"] = {
    version: 0,
    id: new mongoose.Types.ObjectId().toHexString(),
    ticket: {
      id: ticket.id,
    },
  };

  const msg: Partial<Message> = {
    ack: jest.fn(),
  };

  return {
    listener,
    data,
    msg,
  };
}
describe("Order cancelled event listener", () => {
  it("updates ticket ", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);
    const updatedTicket = await Ticket.findById(data.ticket.id);

    expect(updatedTicket).toBeDefined();

    expect(updatedTicket?.orderId).not.toBeDefined();
  });
  it("acks the msg", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);

    expect(msg.ack).toHaveBeenCalled();
  });

  it("publishes ticket event", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);

    expect(natsWrapper.client.publish).toHaveBeenCalled();
    const mock = (natsWrapper.client.publish as jest.Mock).mock.calls;
    const evPayload = JSON.parse(mock[0][1]);

    expect(evPayload.orderId).not.toBeDefined();
  });
});
