import { Listener, OrderCancelledEvent, Subjects } from "@kylab/msvc_shared";
import { Message } from "node-nats-streaming";
import { Ticket } from "../../model/index";
import { TicketUpdatedPublisher } from "./../publisher/ticket-updated-publisher";
import { QUEUE_GRP_NAME } from "./order-created-listener";
export class OrderCancelledEventListener extends Listener<OrderCancelledEvent> {
  readonly subject: Subjects.OrderCancelled = Subjects.OrderCancelled;
  queueGroupName = QUEUE_GRP_NAME;
  async onMsg({ id, ticket }: OrderCancelledEvent["data"], msg: Message) {
    const _ticket = await Ticket.findById(ticket.id);

    if (!_ticket) {
      throw new Error("ticket not found");
    }

    _ticket.set({ orderId: undefined });

    await _ticket.save();

    await new TicketUpdatedPublisher(this.client).publish({
      id: _ticket.id,
      title: _ticket.title,
      price: _ticket.price,
      userId: _ticket.userId,
      version: _ticket.version,
    });

    msg.ack();
  }
}
