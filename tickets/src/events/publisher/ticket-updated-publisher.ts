import { Publisher, Subjects, TicketUpdatedEvent } from "@kylab/msvc_shared";

export class TicketUpdatedPublisher extends Publisher<TicketUpdatedEvent> {
  readonly subject: Subjects.TicketUpdated = Subjects.TicketUpdated;
}
