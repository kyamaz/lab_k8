import { Subjects, TicketCreatedEvent, Publisher } from "@kylab/msvc_shared";

export class TicketCreatedPublisher extends Publisher<TicketCreatedEvent> {
  readonly subject: Subjects.TicketCreated = Subjects.TicketCreated;
}
