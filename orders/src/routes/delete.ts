import {
  OrderStatus,
  protectedRoute,
  throwCustomError,
} from "@kylab/msvc_shared";
import {
  FastifyInstance,
  FastifyRegisterOptions,
  FastifyReply,
  FastifyRequest,
} from "fastify";
import { OrderCancelledPublisher } from "../events/order-cancelled-publisher";
import { Order } from "../model/order";
import { natsWrapper } from "./../nats-wrapper";
interface OrderPayload {
  price: number;
  title: string;
}
type UpdateOrdersRequest = FastifyRequest<{ Body: OrderPayload }>;
export function deleteOrdersRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  fastify.addHook("preHandler", protectedRoute);

  fastify.delete(
    "/api/orders/:id",
    async (request: UpdateOrdersRequest, reply: FastifyReply) => {
      const { id } = request.params as any;

      try {
        let order = await Order.findById(id).populate("ticket");
        if (!order) {
          throwCustomError("not found", "NotFound", 404);
          return;
        }
        if (order.userId !== (request as any)?.activeUser?.id) {
          throwCustomError("not found", "NotAuthorized", 401);
          return;
        }
        order.status = OrderStatus.Cancelled;
        await order.save();

        await new OrderCancelledPublisher(natsWrapper.client).publish({
          id: order.id,
          version: order.version,
          ticket: {
            id: order.ticket.id,
          },
        });

        reply.status(204).send("order cancelled");
      } catch (e) {
        throwCustomError("not found", "NotFound", 404);
      }
    }
  );

  done();
}
