import {
  FastifyInstance,
  FastifyRegisterOptions,
  FastifyReply,
  FastifyRequest,
} from "fastify";
import { Order } from "../model/order";
import { throwCustomError } from "@kylab/msvc_shared";
import { protectedRoute } from "@kylab/msvc_shared";

type ShowOrdersRequest = FastifyRequest;
export function showOrdersRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  fastify.addHook("preHandler", protectedRoute);
  fastify.get(
    "/api/orders/",
    async (request: ShowOrdersRequest, reply: FastifyReply) => {
      try {
        let orders = await Order.find({
          userId: (request as any)?.activeUser?.id,
        }).populate("ticket");

        reply.status(200).send(orders);
      } catch (e) {
        throwCustomError("tickets not found", "NotFound", 404);
      }
    }
  );

  fastify.get(
    "/api/orders/:id",
    async (request: ShowOrdersRequest, reply: FastifyReply) => {
      const { id } = request.params as any;

      try {
        let order = await Order.find({
          userId: (request as any)?.activeUser?.id,
          id,
        }).populate("ticket");
        if (!order) {
          throwCustomError("not found", "NotFound", 404);
        }

        reply.status(200).send(order);
      } catch (e) {
        throwCustomError("not found", "NotFound", 404);
      }
    }
  );

  done();
}
