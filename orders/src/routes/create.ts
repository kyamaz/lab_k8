import {
  OrderStatus,
  protectedRoute,
  throwCustomError,
} from "@kylab/msvc_shared";
import {
  FastifyInstance,
  FastifyRegisterOptions,
  FastifyReply,
  FastifyRequest,
} from "fastify";
import mongoose from "mongoose";
import { OrderCreatedPublisher } from "../events/order-created-publisher";
import { Order } from "../model/order";
import { Ticket } from "../model/tickets";
import { natsWrapper } from "./../nats-wrapper";
interface OrderPayload {
  ticketId: string;
}
type CreateOrdersRequest = FastifyRequest<{ Body: OrderPayload }>;
const EXP_TIME = 40 * 60;
export function createOrdersRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  const schema = {
    body: {
      type: "object",
      required: ["ticketId"],
      properties: {
        ticketId: { type: "string", minLength: 1 },
      },
    },
  };

  fastify.addHook("preHandler", protectedRoute);

  fastify.post(
    "/api/orders/",
    { schema, attachValidation: false },
    async (request: CreateOrdersRequest, reply: FastifyReply) => {
      const { ticketId } = request.body;

      if (!mongoose.Types.ObjectId.isValid(ticketId)) {
        throwCustomError("invalid request", "Bad request", 400);
        return;
      }

      const _ticket = await Ticket.findById(ticketId);
      if (!_ticket) {
        throwCustomError("ticket not found", "Not found", 404);
        return;
      }

      const isReserved = await _ticket.isReserved();

      if (isReserved) {
        throwCustomError("ticket reserved", "Conflict", 409);
        return;
      }
      const exp = new Date();
      exp.setSeconds(exp.getSeconds() + EXP_TIME);
      let order = Order.build({
        ticket: _ticket,
        userId: (request as any)?.activeUser?.id,
        status: OrderStatus.Created,
        expireAt: exp,
      });
      await order.save();

      await new OrderCreatedPublisher(natsWrapper.client).publish({
        id: order.id,
        status: order.status,
        userId: order.userId,
        expiresAt: order.expireAt.toISOString(),
        version: order.version,
        ticket: {
          id: order.ticket.id,
          price: order.ticket.price,
        },
      });
      reply.status(201).send(order);
    }
  );

  done();
}
