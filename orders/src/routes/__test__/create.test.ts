import mongoose from "mongoose";
import { server } from "../../app";
import { Order, OrderStatus } from "../../model/order";
import { Ticket } from "../../model/tickets";
import { natsWrapper } from "./../../nats-wrapper";

//TODO DEMOCK
const MOCK =
  "my-session-cookie=Pvh2aybg45ZgV6MKruuNW68JVLXi31Jjz5t299m9sCs1R0fxROWSCYaWlZ7aeisBknfqlnPylxbeFgqMJ8V3k2Qlgjrz7enx9psjcP1cyNMXzuF%2BLCDQ%2FgRZ3tptwinGXEaS4xZziNPX%2Ffd3kAczcWjSn7aZWWANqAPc8pI6YEbkXBDfu1it%2Bxyj895ZOgr3P3QRy02v4AYK%2FMady1whaMsIEwDvHd%2FqoQEOVJnCPBl7ERldXPK2BaznNLM4jGVn3QPvCVysySO%2BOmeY9Qvx28ew8A%3D%3D%3Bp36myo%2BoJzgpPp2e%2BOYI2EDniEpRWHeK; Path=/; SameSite=Lax";
describe("Create Orders", () => {
  it("return error if ticket does not exist", async () => {
    const ticketId = new mongoose.Types.ObjectId();
    const cookie = await global.signin();
    //TODO demock

    const resp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },
      url: "/api/orders",
      payload: { ticketId },
    });
    expect(resp.statusCode).toEqual(404);
  });
  it("return error if ticket is reserved", async () => {
    const cookie = await global.signin();
    //TODO demock

    const ticket = Ticket.build({
      title: "test",
      price: 12.5,
      id: new mongoose.Types.ObjectId().toHexString(),
    });

    await ticket.save();

    const order = Order.build({
      userId: "ddeededede",
      status: OrderStatus.Complete,
      ticket,
      expireAt: new Date(),
    });

    await order.save();

    const resp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/orders/",
      payload: { ticketId: ticket.id },
    });

    expect(resp.statusCode).toEqual(409);
  });
  it("reserves ticket", async () => {
    const cookie = await global.signin();
    //TODO demock
    const ticket = Ticket.build({
      title: "test",
      price: 12.5,
      id: new mongoose.Types.ObjectId().toHexString(),
    });

    await ticket.save();
    const resp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/orders/",
      payload: { ticketId: ticket.id },
    });

    expect(resp.statusCode).toEqual(201);
  });

  it("emits create event", async () => {
    await global.signin();
    //TODO demock

    const ticket = Ticket.build({
      title: "test",
      price: 12.5,
      id: new mongoose.Types.ObjectId().toHexString(),
    });

    await ticket.save();
    await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/orders/",
      payload: { ticketId: ticket.id },
    });

    expect(natsWrapper.client.publish).toHaveBeenCalled();
  });
});
