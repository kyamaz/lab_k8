import mongoose from "mongoose";
import { server } from "../../app";
import { Ticket } from "../../model/tickets";

//TODO DEMOCK
const MOCK =
  "my-session-cookie=Pvh2aybg45ZgV6MKruuNW68JVLXi31Jjz5t299m9sCs1R0fxROWSCYaWlZ7aeisBknfqlnPylxbeFgqMJ8V3k2Qlgjrz7enx9psjcP1cyNMXzuF%2BLCDQ%2FgRZ3tptwinGXEaS4xZziNPX%2Ffd3kAczcWjSn7aZWWANqAPc8pI6YEbkXBDfu1it%2Bxyj895ZOgr3P3QRy02v4AYK%2FMady1whaMsIEwDvHd%2FqoQEOVJnCPBl7ERldXPK2BaznNLM4jGVn3QPvCVysySO%2BOmeY9Qvx28ew8A%3D%3D%3Bp36myo%2BoJzgpPp2e%2BOYI2EDniEpRWHeK; Path=/; SameSite=Lax";

/* const MOCk2 =
  "my-session-cookie=K%2F98FtkgewuMhCi76Gn8qReFLMlzj1JQRb1OubBb6vQ%2FcbvMLbVsiUDfmjWV7PYXZTMnmF2%2FS8WUnQyOinAfmhui%2Bz5rKKQGnE8kN2%2FXbp6Y%2FKnkdje7kxiRjueY6tjcK1c1reRuoO0vIuT514kwU646zNvB69q1jKaIU0OrFzqPD%2FjJFUYwko9OE7vKN989%2FQxPnFlva3m%2FhBpQdFLhoTA9QuarpOvpWhQidtHNEBlwi7mPh1%2BHiMf4g40mvOFzMeLs%2FPLsJx8wqWXk1Xxrz9EydtWXOUQnvsZc3OLgkQ%3D%3D%3BsUHqKs8NAF5uKb%2BZ27sH4QDVig5Wq%2Bd1; Path=/; SameSite=Lax"; */

async function createTicket() {
  const ticket = Ticket.build({
    title: "test",
    price: 1,
    id: new mongoose.Types.ObjectId().toHexString(),
  });

  await ticket.save();
  return ticket;
}
describe("Show Orders", () => {
  it("fetches order for user", async () => {
    const cookie = await global.signin();
    await global.signin();
    //TODO demock    const ticket = Ticket.build({ title: "test", price: 12.5 });

    const t1 = await createTicket();
    const t2 = await createTicket();
    const t3 = await createTicket();

    await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/orders/",
      payload: { ticketId: t1.id },
    });
    await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/orders/",
      payload: { ticketId: t3.id },
    });
    /*     await server.inject({
      method: "POST",
      headers: {
        cookie: MOCk2,
      },
      url: "/api/orders/",
      payload: { ticketId: t2.id },
    }); */

    const { body: order1 } = await server.inject({
      method: "GET",
      headers: {
        cookie: MOCK,
      },
      url: "/api/orders/",
    });

    expect(JSON.parse(order1).length).toBe(2);
  });

  it("fetches single order for user", async () => {
    const cookie = await global.signin();
    await global.signin();
    //TODO demock    const ticket = Ticket.build({ title: "test", price: 12.5 });

    const t1 = await createTicket();

    const orderResp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/orders/",
      payload: { ticketId: t1.id },
    });
    const orderCreated = JSON.parse(orderResp.body);

    const { body: orderItemResp } = await server.inject({
      method: "GET",
      headers: {
        cookie: MOCK,
      },
      url: "/api/orders/".concat(orderCreated.id),
    });

    const resp = JSON.parse(orderItemResp);
    expect(resp.length).toBe(1);
    expect(resp[0].id).toBe(orderCreated.id);
  });
});
