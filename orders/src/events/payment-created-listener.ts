import {
  Listener,
  OrderStatus,
  PaymentCreatedEvent,
  Subjects,
} from "@kylab/msvc_shared";
import { Message } from "node-nats-streaming";
import { Order } from "../model/order";
import { QUEUE_GRP_NAME } from "./shared";
export class PaymentCreatedListener extends Listener<PaymentCreatedEvent> {
  readonly subject: Subjects.PaymentCreated = Subjects.PaymentCreated;
  queueGroupName = QUEUE_GRP_NAME;
  async onMsg({ orderId }: PaymentCreatedEvent["data"], msg: Message) {
    const order = await Order.findById(orderId);

    if (!order) {
      throw new Error("order not found for payment");
    }
    order.set({ status: OrderStatus.Complete });

    await order.save();

    console.debug(order, "order complete");
    msg.ack();
  }
}
