import { Subjects, OrderCancelledEvent, Publisher } from "@kylab/msvc_shared";

export class OrderCancelledPublisher extends Publisher<OrderCancelledEvent> {
  readonly subject: Subjects.OrderCancelled = Subjects.OrderCancelled;
}
