import { Listener, Subjects, TicketCreatedEvent } from "@kylab/msvc_shared";
import { Message } from "node-nats-streaming";
import { Ticket } from "../model/tickets";
import { QUEUE_GRP_NAME } from "./shared";
export class TicketCreatedListener extends Listener<TicketCreatedEvent> {
  readonly subject: Subjects.TicketCreated = Subjects.TicketCreated;
  queueGroupName = QUEUE_GRP_NAME;
  async onMsg({ id, title, price }: TicketCreatedEvent["data"], msg: Message) {
    const ticket = Ticket.build({
      id,
      title,
      price,
    });

    await ticket.save();

    msg.ack();
  }
}
