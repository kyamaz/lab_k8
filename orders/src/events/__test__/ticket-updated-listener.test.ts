import { TicketUpdatedEvent } from "@kylab/msvc_shared";
import mongoose from "mongoose";
import { Message } from "node-nats-streaming";
import { Ticket } from "../../model/tickets";
import { natsWrapper } from "../../nats-wrapper";
import { TicketUpdatedListener } from "./../ticket-updated-listener";
async function setup() {
  const listener = new TicketUpdatedListener(natsWrapper.client);

  const ticket = Ticket.build({
    id: new mongoose.Types.ObjectId().toHexString(),
    title: "dede",
    price: 77,
  });
  await ticket.save();

  const data: TicketUpdatedEvent["data"] = {
    id: ticket.id,
    version: ticket.version + 1,
    title: "toto",
    price: 12,
    userId: "dede",
  };
  const msg: Partial<Message> = {
    ack: jest.fn(),
  };

  return {
    listener,
    data,
    msg,
  };
}
describe("Update Orders", () => {
  it("find, updates, and save a ticket", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);

    const updatedTicket = await Ticket.findById(data.id);
    expect(updatedTicket).toBeDefined();
    expect(updatedTicket?.title).toBe(data.title);
    expect(updatedTicket?.price).toBe(data.price);
  });
  it("acks the msg", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);

    expect(msg.ack).toHaveBeenCalled();
  });

  it("does not call acl if event has skipped number", async () => {
    const { listener, data, msg } = await setup();

    try {
      data.version = 12;
      await listener.onMsg(data, msg as any);
    } catch (error) {}

    expect(msg.ack).not.toHaveBeenCalled();
  });
});
