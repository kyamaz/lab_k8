import { TicketCreatedEvent } from "@kylab/msvc_shared";
import mongoose from "mongoose";
import { Message } from "node-nats-streaming";
import { natsWrapper } from "../../nats-wrapper";
import { Ticket } from "./../../model/tickets";
import { TicketCreatedListener } from "./../ticket-created-listener";
async function setup() {
  const listener = new TicketCreatedListener(natsWrapper.client);
  const data: TicketCreatedEvent["data"] = {
    version: 0,
    id: new mongoose.Types.ObjectId().toHexString(),
    title: "dede",
    price: 77,
    userId: new mongoose.Types.ObjectId().toHexString(),
  };

  const msg: Partial<Message> = {
    ack: jest.fn(),
  };

  return {
    listener,
    data,
    msg,
  };
}
describe("Create Orders", () => {
  it("create and save a ticket", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);
    const ticket = await Ticket.findById(data.id);
    expect(ticket).toBeDefined();
    expect(ticket?.title).toBe(data.title);
    expect(ticket?.price).toBe(data.price);
  });
  it("acks the msg", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);

    expect(msg.ack).toHaveBeenCalled();
  });
});
