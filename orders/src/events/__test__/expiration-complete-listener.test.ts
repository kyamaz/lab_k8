import { ExpirationCompleteEvent, OrderStatus } from "@kylab/msvc_shared";
import mongoose from "mongoose";
import { Message } from "node-nats-streaming";
import { Order } from "../../model/order";
import { Ticket } from "../../model/tickets";
import { natsWrapper } from "../../nats-wrapper";
import { ExpirationCompleteListener } from "./../expiration-complete-listener";
async function setup() {
  const listener = new ExpirationCompleteListener(natsWrapper.client);

  const ticket = Ticket.build({
    id: new mongoose.Types.ObjectId().toHexString(),
    title: "dede",
    price: 77,
  });
  await ticket.save();

  const order = Order.build({
    userId: "ddede",
    expireAt: new Date(),
    status: OrderStatus.Created,
    ticket,
  });
  await order.save();
  const data: ExpirationCompleteEvent["data"] = {
    id: order.id,
  };

  const msg: Partial<Message> = {
    ack: jest.fn(),
  };

  return {
    listener,
    data,
    msg,
    order,
    ticket,
  };
}
describe("Expiration complete Orders", () => {
  it("updates order status to cancel", async () => {
    const { listener, data, msg, order } = await setup();

    await listener.onMsg(data, msg as any);

    const updatedOrder = await Order.findById(order.id);

    expect(updatedOrder?.status).toBe(OrderStatus.Cancelled);
  });

  it("emit cancel event", async () => {
    const { listener, data, msg, order } = await setup();

    await listener.onMsg(data, msg as any);
    expect(natsWrapper.client.publish).toHaveBeenCalled();

    const evdata = JSON.parse(
      (natsWrapper.client.publish as jest.Mock).mock.calls[0][1]
    );

    expect(evdata.id).toEqual(order.id);
  });

  it("acks the msg", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);

    expect(msg.ack).toHaveBeenCalled();
  });
});
