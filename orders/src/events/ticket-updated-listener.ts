import { Listener, Subjects, TicketUpdatedEvent } from "@kylab/msvc_shared";
import { Message } from "node-nats-streaming";
import { Ticket } from "../model/tickets";
import { QUEUE_GRP_NAME } from "./shared";
export class TicketUpdatedListener extends Listener<TicketUpdatedEvent> {
  readonly subject: Subjects.TicketUpdated = Subjects.TicketUpdated;
  queueGroupName = QUEUE_GRP_NAME;
  async onMsg(
    { id, title, price, version }: TicketUpdatedEvent["data"],
    msg: Message
  ) {
    const ticket = await Ticket.findByEvent({
      id,
      version,
    });

    if (!ticket) {
      throw new Error("ticket nor found ");
    }

    ticket.set({ price, title });
    await ticket.save();

    msg.ack();
  }
}
