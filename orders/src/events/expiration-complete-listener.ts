import {
  ExpirationCompleteEvent,
  Listener,
  OrderStatus,
  Subjects,
} from "@kylab/msvc_shared";
import { Message } from "node-nats-streaming";
import { Order } from "./../model/order";
import { OrderCancelledPublisher } from "./order-cancelled-publisher";
import { QUEUE_GRP_NAME } from "./shared";
export class ExpirationCompleteListener extends Listener<ExpirationCompleteEvent> {
  readonly subject: Subjects.ExpirationComplete = Subjects.ExpirationComplete;
  queueGroupName = QUEUE_GRP_NAME;
  async onMsg({ id }: ExpirationCompleteEvent["data"], msg: Message) {
    const order = await Order.findById(id).populate("ticket");

    if (!order) {
      throw new Error("ticket nor found ");
    }
    if (order.status === OrderStatus.Complete) {
      msg.ack();
      return;
    }

    order.set({ status: OrderStatus.Cancelled });
    await order.save();

    await new OrderCancelledPublisher(this.client).publish({
      id: order.id,
      version: order.version,
      ticket: {
        id: order.ticket.id,
      },
    });
    msg.ack();
  }
}
