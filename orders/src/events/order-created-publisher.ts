import { Subjects, OrderCreatedEvent, Publisher } from "@kylab/msvc_shared";

export class OrderCreatedPublisher extends Publisher<OrderCreatedEvent> {
  readonly subject: Subjects.OrderCreated = Subjects.OrderCreated;
}
