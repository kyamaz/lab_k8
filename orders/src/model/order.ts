import { OrderStatus } from "@kylab/msvc_shared";
import mongoose from "mongoose";
import { updateIfCurrentPlugin } from "mongoose-update-if-current";
import { TicketDoc } from "./tickets";
interface OrdersAttrs {
  ticket: TicketDoc;
  userId: string;
  status: OrderStatus;
  expireAt: Date;
}

interface OrderDoc extends mongoose.Document {
  ticket: TicketDoc;
  userId: string;
  status: OrderStatus;
  expireAt: Date;
  version: number;
}

interface OrderModel extends mongoose.Model<OrderDoc> {
  build(attrs: OrdersAttrs): OrderDoc;
}
const orderSchema = new mongoose.Schema(
  {
    ticket: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Ticket",
    },

    userId: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      required: true,
      enum: Object.values(OrderStatus),
      default: OrderStatus.Created,
    },
    expireAt: {
      type: mongoose.Schema.Types.Date,
    },
  },
  {
    toJSON: {
      transform(_doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);
orderSchema.set("versionKey", "version");
orderSchema.plugin(updateIfCurrentPlugin);
orderSchema.statics.build = (attr: OrdersAttrs) => {
  return new Order(attr);
};
export const Order = mongoose.model<OrderDoc, OrderModel>("Order", orderSchema);
export { OrderStatus };
