import { Listener, OrderCreatedEvent, Subjects } from "@kylab/msvc_shared";
import { Message } from "node-nats-streaming";
import { expirationQueue } from "../../queues/expirationQueue";
export const QUEUE_GRP_NAME = "expiration-service";
export class OrderCreatedListener extends Listener<OrderCreatedEvent> {
  readonly subject: Subjects.OrderCreated = Subjects.OrderCreated;
  queueGroupName = QUEUE_GRP_NAME;
  async onMsg({ id, expiresAt }: OrderCreatedEvent["data"], msg: Message) {
    const delay = new Date(expiresAt).getTime() - new Date().getTime();
    await expirationQueue.add(
      { orderId: id },
      {
        delay,
      }
    );
    msg.ack();
  }
}
