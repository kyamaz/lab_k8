import nats, { Stan } from "node-nats-streaming";

class NatsWrapper {
  private _client?: Stan;
  get client() {
    if (!this._client) {
      throw new Error("client is not defnied");
    }

    return this._client;
  }

  set client(st: Stan) {
    this._client = st;
  }
  constructor() {}

  connect(clusterId: string, clientId: string, url: string) {
    this.client = nats.connect(clusterId, clientId, { url });

    return new Promise<void>((resolve, reject) => {
      this.client.on("connect", () => {
        console.log("nats connected");
        resolve();
      });

      this.client.on("error", (err) => {
        console.log("nats reject", err);

        reject(err);
      });
    });
  }
}

export const natsWrapper = new NatsWrapper();
