import { OrderCreatedListener } from "./events/listeners/order-created-listener";
import { natsWrapper } from "./nats-wrapper";
async function start() {
  try {
    if (!process?.env?.NATS_URL) {
      return new Error("NATS_URL not set");
    }
    if (!process?.env?.NATS_CLUSTER_ID) {
      return new Error("NATS_CLUSTER_IDnot set");
    }
    if (!process?.env?.NATS_CLIENT_ID) {
      return new Error("NATS_CLIENT_ID not set");
    }
    try {
      await natsWrapper.connect(
        process.env.NATS_CLUSTER_ID,
        process.env.NATS_CLIENT_ID,
        process.env.NATS_URL
      );

      new OrderCreatedListener(natsWrapper.client).listen();
      natsWrapper.client.on("close", () => {
        console.log("closing");
        process.exit();
      });
      process.on("SIGINT", () => natsWrapper.client.close());
      process.on("SIGTERM", () => natsWrapper.client.close());
    } catch (e) {
      console.error(e);
    }
  } catch (e) {
    console.error(e);

    throw e;
  }
}
start();
