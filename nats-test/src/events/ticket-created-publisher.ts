import { Publisher, Subjects, TicketCreatedEvent } from "@kylab/msvc_shared";

export class TicketCreatedPublisher extends Publisher<TicketCreatedEvent> {
  readonly subject: Subjects.TicketCreated = Subjects.TicketCreated;
}
