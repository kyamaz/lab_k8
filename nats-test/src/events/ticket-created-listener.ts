import { Message } from "node-nats-streaming";
import { Listener, Subjects, TicketCreatedEvent } from "@kylab/msvc_shared";
export class TicketCreatedListener extends Listener<TicketCreatedEvent> {
  readonly subject: Subjects.TicketCreated = Subjects.TicketCreated;
  queueGroupName = "payments-svc";
  onMsg(data: TicketCreatedEvent["data"], msg: Message) {
    console.log(data);

    msg.ack();
  }
}
