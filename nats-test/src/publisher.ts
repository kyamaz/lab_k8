import nats from "node-nats-streaming";
import { randomBytes } from "node:crypto";
import { TicketCreatedPublisher } from "./events/ticket-created-publisher";
import { TicketCreatedEvent } from "./events/ticket-created-event";

console.clear();

const stan = nats.connect("ticketing", randomBytes(4).toString("hex"), {
  url: "http://localhost:4222",
});

stan.on("connect", async () => {
  console.log("publisher connected to nats");
  stan.on("close", () => {
    console.log("closing");
    process.exit();
  });

  const data: TicketCreatedEvent["data"] = {
    id: "dede",
    title: "deeee",
    price: 12,
  };
  const publisher = new TicketCreatedPublisher(stan);

  try {
    await publisher.publish(data);
  } catch (e) {
    console.error(e);
  }
});
process.on("SIGINT", () => stan.close());
process.on("SIGTERM", () => stan.close());
