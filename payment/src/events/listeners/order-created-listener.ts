import { Listener, OrderCreatedEvent, Subjects } from "@kylab/msvc_shared";
import { Message } from "node-nats-streaming";
import { Order } from "./../../model/order";
export const QUEUE_GRP_NAME = "payment-service";
export class OrderCreatedListener extends Listener<OrderCreatedEvent> {
  readonly subject: Subjects.OrderCreated = Subjects.OrderCreated;
  queueGroupName = QUEUE_GRP_NAME;

  async onMsg(
    {
      id,
      ticket: { price },
      status,
      userId,
      version,
    }: OrderCreatedEvent["data"],
    msg: Message
  ) {
    const order = Order.build({
      id,
      price,
      status,
      userId,
      version,
    });

    await order.save();
    msg.ack();
  }
}
