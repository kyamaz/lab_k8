import {
  Listener,
  OrderCancelledEvent,
  OrderStatus,
  Subjects,
} from "@kylab/msvc_shared";
import { Message } from "node-nats-streaming";
import { Order } from "./../../model/order";
export const QUEUE_GRP_NAME = "payment-service";
export class OrderCancelledListener extends Listener<OrderCancelledEvent> {
  readonly subject: Subjects.OrderCancelled = Subjects.OrderCancelled;
  queueGroupName = QUEUE_GRP_NAME;

  async onMsg({ id, version }: OrderCancelledEvent["data"], msg: Message) {
    const order = await Order.findOne({
      _id: id,
      version: version - 1,
    });

    if (!order) {
      throw new Order("order not found");
    }

    order.set({ status: OrderStatus.Cancelled });
    await order.save();
    msg.ack();
  }
}
