import { OrderCreatedEvent, OrderStatus } from "@kylab/msvc_shared";
import mongoose from "mongoose";
import { Message } from "node-nats-streaming";
import { natsWrapper } from "../../nats-wrapper";
import { Order } from "./../../model/order";
import { OrderCreatedListener } from "./../listeners/order-created-listener";
async function setup() {
  const listener = new OrderCreatedListener(natsWrapper.client);
  const userId = new mongoose.Types.ObjectId().toHexString();

  const data: OrderCreatedEvent["data"] = {
    version: 0,
    id: new mongoose.Types.ObjectId().toHexString(),
    userId,
    status: OrderStatus.Created,
    expiresAt: new Date().toUTCString(),
    ticket: {
      id: new mongoose.Types.ObjectId().toHexString(),
      price: 23,
    },
  };
  const msg: Partial<Message> = {
    ack: jest.fn(),
  };

  return {
    listener,
    data,
    msg,
  };
}
describe("Create Orders Payment", () => {
  it("replicate order info", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);

    const updated = await Order.findById(data.id);
    expect(updated?.price).toBe(data.ticket.price);
  });




  it("acks the msg", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);

    expect(msg.ack).toHaveBeenCalled();
  });
});
