import { OrderCancelledEvent, OrderStatus } from "@kylab/msvc_shared";
import mongoose from "mongoose";
import { Message } from "node-nats-streaming";
import { Order } from "../../model/order";
import { natsWrapper } from "../../nats-wrapper";
import { OrderCancelledListener } from "./../listeners/order-cancelled-listener";
async function setup() {
  const listener = new OrderCancelledListener(natsWrapper.client);
  const userId = new mongoose.Types.ObjectId().toHexString();

  const order = Order.build({
    version: 0,
    id: new mongoose.Types.ObjectId().toHexString(),
    userId,
    status: OrderStatus.Created,
    price: 23,
  });

  await order.save();

  const data: OrderCancelledEvent["data"] = {
    version: 1,
    id: order.id,
    ticket: {
      id: new mongoose.Types.ObjectId().toHexString(),
    },
  };

  const msg: Partial<Message> = {
    ack: jest.fn(),
  };

  return {
    listener,
    data,
    msg,
    order,
  };
}
describe("Create Orders Payment", () => {
  it("cancel order", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);

    const updated = await Order.findById(data.id);
    expect(updated?.status).toBe(OrderStatus.Cancelled);
  });

  it("acks the msg", async () => {
    const { listener, data, msg } = await setup();

    await listener.onMsg(data, msg as any);

    expect(msg.ack).toHaveBeenCalled();
  });
});
