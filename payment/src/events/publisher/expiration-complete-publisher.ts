import {
  ExpirationCompleteEvent,
  Publisher,
  Subjects,
} from "@kylab/msvc_shared";

export class ExpirationCompletePublisher extends Publisher<ExpirationCompleteEvent> {
  readonly subject: Subjects.ExpirationComplete = Subjects.ExpirationComplete;
}
