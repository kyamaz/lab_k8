import { PaymentCreatedEvent, Publisher, Subjects } from "@kylab/msvc_shared";

export class PaymentCreatedPublisher extends Publisher<PaymentCreatedEvent> {
  readonly subject: Subjects.PaymentCreated = Subjects.PaymentCreated;
}
