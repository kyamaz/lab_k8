import { OrderStatus } from "@kylab/msvc_shared";
import mongoose from "mongoose";
import { server } from "../../app";
import { Order } from "./../../model/order";
import { Payment } from "./../../model/payment";
import { stripe } from "./../../stripe";

//TODO DEMOCK

jest.mock("../../stripe.ts");
const MOCK =
  "my-session-cookie=Pvh2aybg45ZgV6MKruuNW68JVLXi31Jjz5t299m9sCs1R0fxROWSCYaWlZ7aeisBknfqlnPylxbeFgqMJ8V3k2Qlgjrz7enx9psjcP1cyNMXzuF%2BLCDQ%2FgRZ3tptwinGXEaS4xZziNPX%2Ffd3kAczcWjSn7aZWWANqAPc8pI6YEbkXBDfu1it%2Bxyj895ZOgr3P3QRy02v4AYK%2FMady1whaMsIEwDvHd%2FqoQEOVJnCPBl7ERldXPK2BaznNLM4jGVn3QPvCVysySO%2BOmeY9Qvx28ew8A%3D%3D%3Bp36myo%2BoJzgpPp2e%2BOYI2EDniEpRWHeK; Path=/; SameSite=Lax";
describe("Create payment", () => {
  it("returns 404 that does not exits", async () => {
    const cookie = await global.signin();

    const resp = await server.inject({
      method: "POST",
      url: "/api/payment/",
      headers: {
        cookie: MOCK,
      },
      payload: {
        orderId: new mongoose.Types.ObjectId().toHexString(),
        token: "rrrrr",
      },
    });

    expect(resp.statusCode).toBe(404);
  });
  it("returns 401 when purchasing order belonging to anoter one", async () => {
    const order = Order.build({
      id: new mongoose.Types.ObjectId().toHexString(),
      userId: new mongoose.Types.ObjectId().toHexString(),
      version: 0,
      price: 22,
      status: OrderStatus.Created,
    });

    await order.save();
    const resp = await server.inject({
      method: "POST",
      url: "/api/payment/",
      headers: {
        cookie: MOCK,
      },
      payload: {
        orderId: order.id,
        token: "rrrrr",
      },
    });

    expect(resp.statusCode).toBe(401);
  });
  it("returns 400 when purchasing cancelled ticket", async () => {
    //TODO demock
    const cookie = await global.signin();

    const order = Order.build({
      id: new mongoose.Types.ObjectId().toHexString(),
      userId: "62997e6d207766eac12c291d", //new mongoose.Types.ObjectId().toHexString(),
      version: 0,
      price: 22,
      status: OrderStatus.Cancelled,
    });

    await order.save();
    const resp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/payment/",
      payload: {
        orderId: order.id,
        token: "rrrrr",
      },
    });

    expect(resp.statusCode).toBe(400);
  });
  it("returns 201 when purchasing cancelled ticket", async () => {
    //TODO demock
    const cookie = await global.signin();

    const order = Order.build({
      id: new mongoose.Types.ObjectId().toHexString(),
      userId: "62997e6d207766eac12c291d",
      version: 0,
      price: 22,
      status: OrderStatus.Created,
    });

    await order.save();
    const resp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/payment/",
      payload: {
        orderId: order.id,
        token: "tok_visa",
      },
    });

    expect(resp.statusCode).toBe(201);

    const chargeOpt = (stripe.charges.create as jest.Mock).mock.calls[0][0];

    expect(chargeOpt.source).toBe("tok_visa");
    expect(chargeOpt.amount).toBe(order.price * 100);
    expect(chargeOpt.currency).toBe("usd");
  });
  it("save payment on sucessful charges", async () => {
    //TODO demock
    const cookie = await global.signin();

    const order = Order.build({
      id: new mongoose.Types.ObjectId().toHexString(),
      userId: "62997e6d207766eac12c291d",
      version: 0,
      price: 22,
      status: OrderStatus.Created,
    });

    await order.save();
    const resp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/payment/",
      payload: {
        orderId: order.id,
        token: "tok_visa",
      },
    });

    expect(resp.statusCode).toBe(201);

    const payment = await Payment.findOne({
      orderId: order.id,
      stripeId: "testid",
    });

    expect(payment).not.toBeNull();
  });
  /*   it("publishes event", async () => {
    let tickets = await Ticket.find({});

    expect(tickets.length).toEqual(0);

    const cookie = await global.signin();
    //TODO demock

    const resp = await server.inject({
      method: "POST",
      headers: {
        cookie: MOCK,
      },

      url: "/api/tickets/",
      payload: { title: "test", price: 12.5 },
    });

    expect(natsWrapper.client.publish).toHaveBeenCalled();
  }); */
});
