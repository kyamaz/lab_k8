import {
  OrderStatus,
  protectedRoute,
  throwCustomError,
} from "@kylab/msvc_shared";
import {
  FastifyInstance,
  FastifyRegisterOptions,
  FastifyReply,
  FastifyRequest,
} from "fastify";
import mongoose from "mongoose";
import { natsWrapper } from "../nats-wrapper";
import { PaymentCreatedPublisher } from "./../events/publisher/payment-created";
import { Order } from "./../model/order";
import { Payment } from "./../model/payment";
import { stripe } from "./../stripe";

interface ChargePayload {
  orderId: string;
  token: string;
}
type CreateChargeRequest = FastifyRequest<{ Body: ChargePayload }>;
export function createChargeRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  const schema = {
    body: {
      type: "object",
      required: ["orderId", "token"],
      properties: {
        orderId: { type: "string", minLength: 1 },
        token: { type: "string", minLength: 1 },
      },
    },
  };

  fastify.addHook("preHandler", protectedRoute);

  fastify.post(
    "/api/payment/",
    { schema, attachValidation: false },
    async (request: CreateChargeRequest, reply: FastifyReply) => {
      const { orderId, token } = request.body;

      if (!mongoose.Types.ObjectId.isValid(orderId)) {
        throwCustomError("invalid request", "Bad request", 400);
        return;
      }

      const _order = await Order.findById(orderId);

      if (!_order) {
        throwCustomError("ticket not found", "Not found", 404);
        return;
      }
      if (_order.userId !== (request as any)?.activeUser?.id) {
        throwCustomError("not found", "NotAuthorized", 401);
        return;
      }
      if (_order.status === OrderStatus.Cancelled) {
        throwCustomError("order cancelled", "Bad request", 400);
        return;
      }
      if (_order.status === OrderStatus.Complete) {
        throwCustomError("order already paid", "Bad request", 400);
        return;
      }

      const charge = await stripe.charges.create({
        amount: _order.price * 100,
        currency: "usd",
        source: token,
      });

      const p = Payment.build({
        stripeId: charge.id,
        orderId: _order.id,
      });

      _order.set({ status: OrderStatus.Complete });
      try {
        await p.save();
        await _order.save();
      } catch (error) {
        throwCustomError("failed payment", "Bad request", 400);
      }

      await new PaymentCreatedPublisher(natsWrapper.client).publish({
        orderId: p.orderId,
        stripeId: p.stripeId,
        id: p.id,
      });
      reply.status(201).send({ id: p.id });
    }
  );

  done();
}
