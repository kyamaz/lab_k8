import cors from "@fastify/cors";
import secureSession from "@fastify/secure-session";
// eslint-disable-next-line
import { activeUserDecorator, setApiErrorResp } from "@kylab/msvc_shared";
import fastify, { FastifyRequest } from "fastify";
import * as fs from "fs";
import * as path from "path";
import { createChargeRoute } from "./routes/new";

declare module "@fastify/secure-session" {
  interface SessionData {
    jwt: string;
  }
}

const server = fastify();

server.register(cors as any, {
  origin: "*",
  method: ["GET", "POST"],
  credentials: true,
  exposedHeaders: ["Set-Cookie"],
});
server.register(secureSession, {
  cookieName: "my-session-cookie",
  // WARN must be 32 bytes excatly
  key: fs.readFileSync(path.join(__dirname, "secret-key")),
  cookie: {
    httpOnly: false, //process.env.NODE_ENV !== "test", // Use httpOnly for all production purposes
    sameSite: "lax",
    secure: false,
    signed: false,
    path: "/",
  },
});

//custom plugin attach auth user or null to request prop user
server.register(activeUserDecorator);

// global error response handler
server.setErrorHandler(function (error, _request, reply) {
  //  console.warn(error);
  const { status, message, errorName } = setApiErrorResp(error);
  reply.status(status).send({ message, statusCode: status, error: errorName });
});

//ROUTES
server.register(createChargeRoute);

server.get("*", function (req: FastifyRequest, res: any) {
  console.debug(req.url, "url");
  res.status(404).send({
    message: req.url.concat(" not found"),
    statusCode: "404",
    error: "NOT FOUND",
  });
});
export { server };
