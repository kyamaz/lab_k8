import { MongoMemoryServer } from "mongodb-memory-server";
import mongoose from "mongoose";
import jwt from "jsonwebtoken";
import { toHash } from "@kylab/msvc_shared";

declare global {
  var signin: () => Promise<any>;
}

jest.mock("./../nats-wrapper");

let mongo: any;
beforeAll(async () => {
  process.env.jwt = "test";
  mongo = new MongoMemoryServer();
  await mongo.start();
  const mongoUri = mongo.getUri();
  await mongoose.connect(mongoUri);
});

beforeEach(async () => {
  jest.clearAllMocks();
  const collections = await mongoose.connection.db.collections();
  for (let col of collections) {
    await col.deleteMany({});
  }
});
afterAll(async () => {
  await mongoose.connection.close();
  await mongo.stop();
});

global.signin = async function (id?: string | undefined) {
  const uSwt = jwt.sign(
    {
      email: "test@test.dededed",
      id: id ?? "62997355e23453f680e298e7",
    },
    process?.env?.jwt ?? ""
  );

  const mock = JSON.stringify({ jwt: uSwt });
  const h = await toHash(mock);

  return "my-session-cookie=".concat(
    Buffer.from(h).toString("base64"),
    "; Path=/; SameSite=Lax"
  );
};
