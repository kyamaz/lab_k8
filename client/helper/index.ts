export function getBaseUrl() {
  return typeof window === "undefined"
    ? "http://ingress-nginx-controller.ingress-nginx.svc.cluster.local" // service.namespace.sv.cluster.local. can set external name service
    : "";
}
export async function makeRequest({ req }: any, apiUrl: string) {
  const baseUrl = getBaseUrl();
  const headers: any = req?.headers ?? {};

  try {
    console.log(baseUrl, "ddedeed");
    const data = await fetch(baseUrl.concat(apiUrl), {
      method: "GET",
      mode: "cors",
      credentials: "include",
      headers: {
        ...headers,
      },
    }).then((res) => {
      return res.json();
    });

    return data;
  } catch (e) {
    console.warn("err", e);
    return null;
  }
}
