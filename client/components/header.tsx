import Link from "next/link";
export function Header({ user }: { user: null | any }) {
  const links = [
    !user && {
      label: "sign in",
      href: "/auth/signin",
    },
    !user && {
      label: "sign up",
      href: "/auth/signup",
    },
    user && {
      label: "sign out",
      href: "/auth/signout",
    },
  ].filter(Boolean);

  return (
    <nav className="navbar navbar-light bg-light">
      <Link href="/">
        <a className="navbar-brand">gitix</a>
      </Link>
      <Link href="/tickets/new">
        <a className="navbar-brand">tickets</a>
      </Link>
      <div className="d-flex justify-content-end">
        <ul className="nav d-flex align-items-center">
          {links.map((l) => {
            return (
              <Link href={l.href} key={l.href}>
                <a className="navbar-brand">{l.label}</a>
              </Link>
            );
          })}
        </ul>
      </div>
    </nav>
  );
}
//
