import { useState } from "react";

export default function (
  url: string,
  option: any = {},
  successHandle = (resp: any) => resp
) {
  const [error, setError] = useState<null | any>(null);
  async function doRequest(args?: any) {
    return fetch(window?.location?.origin.concat(url), {
      method: "GET",
      cache: "default",
      headers: {
        "Content-Type": "application/json",
      },
      credentials: "include",
      ...option,
      body: JSON.stringify({
        ...(option?.body ?? {}),
        ...args,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((resp: any) => {
        if (resp.error) {
          throw resp;
        }
        setError(null);
        //TODO chcek my setcokkie does not document.cookie
        if (resp.temp) {
          document.cookie = "my-session-cookie=".concat(resp.temp);
        }
        successHandle(resp);
      })
      .catch((e: any) => {
        console.warn(e, "catch");
        setError(
          <div className="alert alert-danger">
            <h4>Oups </h4>
            <p>{e.message}</p>
          </div>
        );
      });
  }

  return { doRequest, error };
}
