import type { AppProps } from "next/app";
import "../styles/globals.css";
import { Header } from "./../components/header";
import { makeRequest } from "./../helper/index";

const AppComponent = ({ Component, pageProps }: AppProps) => {
  const ugly = pageProps?.user?.currentUser
    ? pageProps.user.currentUser
    : pageProps.user;
  const p = { ...pageProps, user: ugly };
  return (
    <div>
      <Header user={ugly} />
      <Component {...p} />
    </div>
  );
};
AppComponent.getInitialProps = async ({ Component, ctx }: any) => {
  const data = await makeRequest(ctx, "/api/users/currentuser");

  return {
    pageProps: { user: data?.error ? null : data },
  };
};

export default AppComponent;
