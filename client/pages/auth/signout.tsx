import { useEffect } from "react";
import useRequest from "./../../hooks/use-request";
import Router from "next/router";
export default function () {
  const { doRequest } = useRequest(
    "/api/users/signout",
    {
      method: "POST",
    },
    async () => {
      //Router.push("/")
      document.cookie = "";
      Router.push("/");
    }
  );

  useEffect(() => {
    doRequest();
  }, []);
  return <div> signing you out ....</div>;
}
