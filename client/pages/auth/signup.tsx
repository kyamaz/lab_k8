import { useState } from "react";
import useRequest from "./../../hooks/use-request";
import Router from "next/router";
export default function () {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { doRequest, error } = useRequest(
    "/api/users/signup",
    {
      method: "POST",
      body: {
        email,
        password,
      },
      credentials: "include",
      mode: "cors",
    },
    async () => {
      //Router.push("/")
      Router.push("/");
    }
  );

  function handleSubmit(e: any) {
    e.preventDefault();

    doRequest();
  }
  return (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <label>Email Address</label>
        <input
          className="form-control"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          type="email"
        />
      </div>
      <div className="form-group">
        <label>Password</label>
        <input
          type="password"
          className="form-control"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>
      <button type="submit" className="btn btn-primary">
        Sign Up
      </button>

      {error}
    </form>
  );
}
