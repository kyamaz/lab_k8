import Router from "next/router";
import { useEffect, useRef, useState } from "react";
import { makeRequest } from "../../helper";
import useRequest from "../../hooks/use-request";
import StripeCheckout from "react-stripe-checkout";
export default function ({ order, user }: any) {
  const [timeLeft, setTimeLeft] = useState(0);
  const [orderComplete, setOrderComplete] = useState(false);

  const { doRequest, error } = useRequest(
    "/api/payment/",
    {
      method: "POST",
      body: {
        orderId: order.id,
      },
      credentials: "include",
      mode: "cors",
    },
    async (order: any) => {
      console.log(order);
      setOrderComplete(true);
    }
  );
  useEffect(() => {
    const getTimeLeft = () => {
      const msLeft = new Date(order?.expireAt).getTime() - new Date().getTime();
      setTimeLeft(Math.round(msLeft / 1000));
    };
    getTimeLeft();
    const i = setInterval(getTimeLeft, 1000);
    return () => {
      clearInterval(i);
    };
  }, []);

  function purchase(token: any) {
    console.log(token);

    console.log(token);

    doRequest({ token: token.id });
  }

  return (
    <div className="col-8">
      <h1>order: {order?.id}</h1>
      <p>
        {orderComplete
          ? "order complete"
          : `you have ${timeLeft} secondes left until order expires`}
      </p>

      <StripeCheckout
        email={user.email}
        amount={(order?.ticket?.price ?? 0) * 100}
        token={purchase}
        stripeKey="pk_test_51LHfr7IAWKMvSZr7BjDcLpL7mf1ihzuPhFW5hRQRQSuBevef9Sw1CNhZRwuBgyM42ttHFidxQtvsA175AHqIAAYG00M49Q1WzE"
      />
      {error}
    </div>
  );
}
export async function getServerSideProps(context: any) {
  const id = context.params?.orderid;
  if (!id) {
    return { props: { order: null } };
  }

  const order = await makeRequest(context, "/api/orders/".concat(id));

  const _o = order.find((el: any) => el.id === id);

  console.log(order, "dede");
  return {
    props: {
      order: _o ?? null,
    }, // will be passed to the page component as props
  };
}
