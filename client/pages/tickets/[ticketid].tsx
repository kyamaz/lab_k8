import Router from "next/router";
import { useState } from "react";
import { makeRequest } from "../../helper";
import useRequest from "../../hooks/use-request";

export default function ({ ticket }: any) {
  const [title, setTitle] = useState(ticket.title ?? "");
  const [price, setPrice] = useState(ticket.price ?? "");
  const { doRequest, error } = useRequest(
    "/api/tickets/",
    {
      method: "PUT",
      body: {
        title,
        price,
      },
      credentials: "include",
      mode: "cors",
    },
    async (ticket: any) => {
      console.log(ticket);
      Router.push("/");
    }
  );

  const { doRequest: buyRequest, error: buyError } = useRequest(
    "/api/orders/",
    {
      method: "POST",
      body: {
        ticketId: ticket.id ?? "",
      },
      credentials: "include",
      mode: "cors",
    },
    async (order: any) => {
      console.log(order);

      Router.push("/orders/[orderid]", `/orders/${order.id}`);
    }
  );
  function handleSubmit(e: any) {
    e.preventDefault();

    doRequest();
  }

  function onblur(_price: string) {
    const value = parseFloat(_price);
    if (isNaN(value)) {
      setPrice("");

      return "";
    }
    setPrice(value.toFixed(2));
  }

  function purchase(e: any) {
    buyRequest();
  }
  return (
    <div className="col-8">
      <h1>update ticket: {ticket?.title}</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>Title</label>
          <input
            className="form-control"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label>Price</label>
          <input
            className="form-control"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            onBlur={(e) => onblur(e.target.value)}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          update
        </button>
        <button type="button" className="btn btn-primary" onClick={purchase}>
          order
        </button>
        {error}
      </form>
    </div>
  );
}
export async function getServerSideProps(context: any) {
  const id = context.params?.ticketid;
  console.debug(id, context.params);
  if (!id) {
    return { props: { ticket: null } };
  }

  const ticket = await makeRequest(context, "/api/tickets/".concat(id));

  return {
    props: {
      ticket,
    }, // will be passed to the page component as props
  };
}
