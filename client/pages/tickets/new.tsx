import Router from "next/router";
import { useState } from "react";
import useRequest from "../../hooks/use-request";

export default function () {
  const [title, setTitle] = useState("");
  const [price, setPrice] = useState("");
  const { doRequest, error } = useRequest(
    "/api/tickets/",
    {
      method: "POST",
      body: {
        title,
        price,
      },
      credentials: "include",
      mode: "cors",
    },
    async (ticket: any) => {
      console.log(ticket);
      Router.push("/");
    }
  );

  function handleSubmit(e: any) {
    e.preventDefault();

    doRequest();
  }

  function onblur(_price: string) {
    const value = parseFloat(_price);
    if (isNaN(value)) {
      setPrice("");

      return "";
    }
    setPrice(value.toFixed(2));
  }
  return (
    <div className="col-8">
      <h1>Create ticket</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>Title</label>
          <input
            className="form-control"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label>Price</label>
          <input
            className="form-control"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            onBlur={(e) => onblur(e.target.value)}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Sign in
        </button>

        {error}
      </form>
    </div>
  );
}
