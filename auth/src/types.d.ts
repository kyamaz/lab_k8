import fastify from "fastify";
declare module "fastify" {
  export interface FastifyRequest {
    activeUser: { id: string; email: string };
    session: Object;
  }
}
