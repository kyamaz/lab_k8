import { MongoMemoryServer } from "mongodb-memory-server";
import mongoose from "mongoose";
import { server } from "../app";
declare global {
  var signin: () => Promise<any>;
}
let mongo: any;
beforeAll(async () => {
  process.env.jwt = "test";
  mongo = new MongoMemoryServer();
  await mongo.start();
  const mongoUri = mongo.getUri();
  await mongoose.connect(mongoUri);
});

beforeEach(async () => {
  const collections = await mongoose.connection.db.collections();
  for (let col of collections) {
    await col.deleteMany({});
  }
});
afterAll(async () => {
  await mongoose.connection.close();
  await mongo.stop();
});

global.signin = async () => {
  const authresp = await server.inject({
    method: "POST",
    url: "/api/users/signup",
    payload: { email: "johnson@gmail.com", password: "123IIIII" },
  });

  const cookie = authresp.headers["set-cookie"];

  return cookie;
};
