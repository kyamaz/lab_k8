import mongoose from "mongoose";
import { server } from "./app";

async function start() {
  try {
    if (!process?.env?.jwt) {
      return new Error("env not set");
    }
    if (!process?.env?.MONGO_URI) {
      return new Error("mongo uri not set");
    }
    try {
      await mongoose.connect(process.env.MONGO_URI);
    } catch (e) {
      console.error(e);
    }

    server.listen(3000, "0.0.0.0", (err, address) => {
      if (err) {
        console.error(err);
        process.exit(1);
      }
      console.log(`auth server listening at ${address} port 3000.`);
    });
  } catch (e) {
    throw e;
  }
}
start();
