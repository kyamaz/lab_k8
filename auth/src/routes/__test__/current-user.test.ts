import { server } from "../../app";
describe("Current user", () => {
  it("return 200 on success", async () => {
    const cookie = await signin();
    const resp = await server.inject({
      headers: { cookie },
      method: "GET",
      url: "/api/users/currentuser",
    });

    expect(resp.statusCode).toBe(200);
    expect(JSON.parse(resp.body)?.currentUser?.email).toBe("johnson@gmail.com");
  });

  it("return 401 on non auth", async () => {
    const resp = await server.inject({
      method: "GET",
      url: "/api/users/currentuser",
    });

    expect(resp.statusCode).toBe(401);
  });
});
