import { server } from "../../app";
describe("Signup user", () => {
  it("return 201 on success", async () => {
    const resp = await server.inject({
      method: "POST",
      url: "/api/users/signup",
      payload: { email: "johnson@gmail.com", password: "123IIIII" },
    });

    expect(resp.statusCode).toBe(201);
  });

  it("return 400 on invalid payload", async () => {
    const resp = await server.inject({
      method: "POST",
      url: "/api/users/signup",
      payload: {},
    });

    expect(resp.statusCode).toBe(400);
  });

  it("return 400 on invalid email", async () => {
    const resp = await server.inject({
      method: "POST",
      url: "/api/users/signup",
      payload: { email: "johnsongmail.com", password: "123IIIII" },
    });

    expect(resp.statusCode).toBe(400);
  });

  it("return 400 on invalid pwd", async () => {
    const resp = await server.inject({
      method: "POST",
      url: "/api/users/signup",
      payload: { email: "johnsongmail.com", password: "1" },
    });

    expect(resp.statusCode).toBe(400);
  });

  it("disallow duplicate email", async () => {
    await server.inject({
      method: "POST",
      url: "/api/users/signup",
      payload: { email: "johnson@gmail.com", password: "validpwd" },
    });

    const resp = await server.inject({
      method: "POST",
      url: "/api/users/signup",
      payload: { email: "johnson@gmail.com", password: "123IIIII" },
    });

    expect(resp.statusCode).toBe(409);
  });

  it("set cookie after succesful request", async () => {
    const resp = await server.inject({
      method: "POST",
      url: "/api/users/signup",
      payload: { email: "johnson@gmail.com", password: "123IIIII" },
    });

    expect(resp.headers["set-cookie"]).toBeDefined();
  });
});
