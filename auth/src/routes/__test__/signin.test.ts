import { server } from "../../app";
describe("Signin user", () => {
  it("return 200 on success", async () => {
    await server.inject({
      method: "POST",
      url: "/api/users/signup",
      payload: { email: "johnson@gmail.com", password: "123IIIII" },
    });

    const resp = await server.inject({
      method: "POST",
      url: "/api/users/signin",
      payload: { email: "johnson@gmail.com", password: "123IIIII" },
    });

    expect(resp.headers["set-cookie"]).toBeDefined();
    expect(resp.statusCode).toBe(200);
  });

  it("return 400 on email or pwd that does not exist", async () => {
    const resp = await server.inject({
      method: "POST",
      url: "/api/users/signin",
      payload: { email: "johnson@gmail.com", password: "123IIIII" },
    });

    expect(resp.statusCode).toBe(400);
  });

  it("return 400 on invalid payload", async () => {
    const resp = await server.inject({
      method: "POST",
      url: "/api/users/signin",
      payload: {},
    });

    expect(resp.statusCode).toBe(400);
  });

  it("return 400 on invalid email", async () => {
    const resp = await server.inject({
      method: "POST",
      url: "/api/users/signin",
      payload: { email: "johnsongmail.com", password: "123IIIII" },
    });

    expect(resp.statusCode).toBe(400);
  });

  it("return 400 on invalid pwd", async () => {
    const resp = await server.inject({
      method: "POST",
      url: "/api/users/signin",
      payload: { email: "johnsongmail.com", password: "1" },
    });

    expect(resp.statusCode).toBe(400);
  });
});
