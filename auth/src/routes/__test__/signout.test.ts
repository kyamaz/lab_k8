import { server } from "../../app";
describe("Signout user", () => {
  it("return 200 on success", async () => {
    const cookie = await signin();

    const resp = await server.inject({
      method: "POST",
      headers: { cookie },
      url: "/api/users/signout",
      payload: {},
    });

    expect(resp.statusCode).toBe(200); // TODO should be 200
  });
});
