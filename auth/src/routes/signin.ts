import { UserRequesPayload } from "../model";
import { User } from "../model/user";
import { FastifyInstance, FastifyReply, FastifyRequest } from "fastify";
import jwt from "jsonwebtoken";
// eslint-disable-next-line
import {
  UserAuthenticateSchema,
  throwCustomError,
  compare,
} from "@kylab/msvc_shared";
const schema = {
  body: UserAuthenticateSchema,
};
type UserSigninRequest = FastifyRequest<{ Body: UserRequesPayload }>;

export function signInUsertRoute(
  fastify: FastifyInstance,
  _: any,
  done: Function
) {
  fastify.post(
    "/api/users/signin",
    { schema, attachValidation: false },
    async (request: UserSigninRequest, reply: FastifyReply) => {
      const { email, password } = request.body as UserRequesPayload;

      const existingUser = await User.findOne({ email });

      if (!existingUser) {
        throwCustomError("bad request", "bad request", 400);
        return;
      }

      const isPwdValid = await compare(existingUser.password, password);

      if (!isPwdValid) {
        throwCustomError("invalid creds", "auth", 401);
        return;
      }
      const uSwt = jwt.sign(
        {
          email,
          id: existingUser.id,
        },
        process?.env?.jwt ?? ""
      );

      if ((request as any).session) {
        (request as any).session.set("jwt", uSwt);
      }

      reply.status(200).send(existingUser);
    }
  );

  done();
}
