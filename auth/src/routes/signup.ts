import {
  FastifyInstance,
  FastifyRegisterOptions,
  FastifyReply,
  FastifyRequest,
} from "fastify";
import { UserRequesPayload } from "../model";
import { User } from "../model/user";

import jwt from "jsonwebtoken";

import { UserAuthenticateSchema, throwCustomError } from "@kylab/msvc_shared";

type UserSingupRequest = FastifyRequest<{ Body: UserRequesPayload }>;
export function signupUsertRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  const schema = {
    body: UserAuthenticateSchema,
  };
  fastify.post(
    "/api/users/signup",
    { schema, attachValidation: false },
    async (request: UserSingupRequest, reply: FastifyReply) => {
      const { email, password } = request.body as UserRequesPayload;

      const existsUser = await User.findOne({ email });
      if (existsUser) {
        throwCustomError("already exists", "conflict", 409);
      }

      const user = User.build({ email, password });

      await user.save();

      const uSwt = jwt.sign(
        {
          email,
          id: user.id,
        },
        process?.env?.jwt ?? ""
      );

      if (request.session) {
        request.session.set("jwt", uSwt);
      }

      reply.status(201).send({
        email,
        id: user.id,
        temp: uSwt,
      });
    }
  );

  done();
}
