import { FastifyInstance, FastifyReply, FastifyRequest } from "fastify";

// eslint-disable-next-line
import { protectedRoute } from "@kylab/msvc_shared";
export function signOutUsertRoute(
  fastify: FastifyInstance,
  _: any,
  done: Function
) {
  fastify.addHook("preHandler", protectedRoute);

  fastify.post(
    "/api/users/signout",
    async (request: FastifyRequest, reply: FastifyReply) => {
      if (request.session) {
        request.session.delete();
      }
      reply.status(200).send("signed out");
    }
  );

  done();
}
