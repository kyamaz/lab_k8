import { FastifyInstance, FastifyReply, FastifyRequest } from "fastify";

// eslint-disable-next-line
import { protectedRoute } from "@kylab/msvc_shared";
export function currentUsertRoute(
  fastify: FastifyInstance,
  _: any,
  done: Function
) {
  fastify.addHook("preHandler", protectedRoute);

  fastify.get(
    "/api/users/currentuser",
    async (request: FastifyRequest, reply: FastifyReply) => {
      reply.status(200).send({
        currentUser: (request as any).activeUser,
      });
    }
  );
  done();
}
