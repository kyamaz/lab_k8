export const UserAuthenticateSchema = {
  type: "object",
  required: ["password", "email"],
  properties: {
    email: { type: "string", format: "email" },
    password: {
      minLength: 5,
      not: {
        enum: ["null"],
      },
    },
  },
};
