import mongoose from "mongoose";
import { toHash } from "@kylab/msvc_shared";
import { Schema } from "mongoose";
interface UserAttrs {
  email: string;
  password: string;
}

interface UserDocument<T> extends mongoose.Document<T> {}
interface UserModel<T> extends mongoose.Model<T> {
  build: (attrs: T) => UserDocument<T>;
}
const UserSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc: UserDocument<UserAttrs>, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.password;
        delete ret.__v;
      },
    },
  }
);

UserSchema.pre("save", async function (done: Function) {
  if (this.isModified("password")) {
    const hashed = await toHash(this.get("password"));
    this.set("password", hashed);
  }
  done();
});
UserSchema.statics.build = function (attrs: UserAttrs) {
  return new User(attrs);
};
export const User = mongoose.model<
  UserDocument<UserAttrs>,
  UserModel<UserAttrs>
>("User", UserSchema);
